# cours blender cambre 2021

presentation:

- frankiezafe.org / wiki.frankiezafe.org (tri-lamp) / polymorph.cool / polymorph.sh / zur.glub.live
- blender-brussels >> https://blender-brussels.github.io
- blender (?GPL)
- https://mastodon.xyz/@frankiezafe
- https://twitter.com/frankiezafe

- examples + related projects
 - gears
 - b4designer
 - tri-lamp
 - whistles 
 - soft-cube
 - inkdrops
 - joan

- shiv integer >> https://www.thingiverse.com/shivinteger

- Goodbye Uncanny Valley > https://vimeo.com/237568588

- meeting w. students

## administration / communication:

tableau des présences: https://lite.framacalc.org/9pvo-b3d_lacambre_2021

pad reprenant les notes: https://annuel2.framapad.org/p/b3d_lacambre_2021-9pvo?lang=en

- framacalc pour les présences
- framapad pour les notes
- repo gtilab pour le cours

resources:

- https://blender-brussels.github.io
- https://wiki.imal.org/fuzzysearch/results/blender
- https://anwiki.artsaucarre.be/index.php?title=Glitch_3D
- https://blendswap.com
- https://blendermada.com
- https://textures.com
- https://technofle.sh
- https://www.mixamo.com / https://substance3d.adobe.com/plugins/mixamo-in-blender/

## organisation of the sessions

- #01: interface de blender 15 minutes
- #02: modelisation (bases): 3h45
 - importation d'objets
 - manipulation d'objects: duplication, groupes, collections
 - object mode / edit mode
 - travail avec la géométrie: création de faces, division, extrusion
 - modificateurs
 - lowpoly
- #03: rendu (bases)
 - caméras
 - lumières
 - matériaux simples
- #04: rendus bitmap + rendus SVG
 - eevee et cycle
 - freestyle
- #05: photo réalisme > cycle, HDRI
 - cycle
 - HDRIs
 - shaders: matériaux et rendus avancés
- #06: modelisation (sculpting)
 - présentation des différents outils de sculpting
- #07: exportation: formats de sorties et web
- #08: animation bases
 - timeline et graph editor
 - NLA
- #09: animation squelettale
 - rigging & skinning
- #10: impression 3D
 - export STL + verification dans MeshLab
- #11: 3D scan
 - à organiser avec le fablab
- #12: physics simulation
 - particles 
 - cloth
 - softbodies
 - fluid
- #13: camera tracking
- #14: génératif & procédural
 - geometry nodes
 - python scripting 


