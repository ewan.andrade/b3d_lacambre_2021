
# Cours de 3D B3



[https://gitlab.com/frankiezafe/b3d\_lacambre\_2021](**https://gitlab.com/frankiezafe/b3d\_lacambre\_2021**)

[https://download.blender.org/release/](https://download.blender.org/release/)



étudiants:



Paloma Plana

   * Typographie: 
   * Modélisation 3D typographique, du fixe à l'animation.
   * Export Web
Clement Delepierre

   * Typo: Découvrir la 3d sans trop savoir vers quoi aller même si le travail d'image fixe me plait bien. 
   * Export web \& 3D scan
Amélie Tricaud

   * Photographie : intégrer à mon travail de photographie de l'image 3D animée et fixe (photo réalisme). <<<3 :"33
Anatole Mélot

   * Photographie, j'aimerais explorer le monde de la 3D en image fixe ou en animation ( J'ai souvent du mal à prendre en main un logiciel (dernier canard de file <3 ))
Zélie Dierckx

   * Com. graph.
   * Découvrir différentes possibilités en 3d, image, fixe, animation et web et voir comment lier ça à ma pratique du graphisme.
Emma Bianchi :

   * Architecture d'intérieur, bases en cinéma 4D, intérêt pour l'animation, simulation de fluide, incrustation, création/rendu d'espace, de matière, lumière, photoréalisme?
Lolà Mancini :

   * architecture d’intérieur, rendus 3D réalistes matérialité lumière, animations
Ewan Andrade Lopes

   * Comgraph Découvrir la 3D, modelisation, animation procedural
Lune Jusseau

   * photographie.  image fixe et création de vidéo avec des animations d'humains dans des environnements réalistes 
Clémentine Bost

   * Com graph : découvrir la 3D autant fixe que en animation en lien avec le graphisme, intéressé par l'export web aussi 
Matthieu DUPILLE

   * Cinéma d'animation : modélisation, animation, création de fichier pour impression, scan 3D
Gaïa Ingargiola

   * stylisme: scan des pièces, impression 3D, modélisation de l'espace
Hyunju Park

   * scan des pièces, impression 3D, modélisation de l'espace
Eve GARANGER:

   * Design textile, rendu de matière et texture, rapport entre le textile et le corp travail de la lumière (reflets brillance)/ impression 3D
TOM RAMBAUD  

   * stylisme : scan , modélisation impression d'objets d'espace et motif? 
William Denis

   * ComGraph, j'aime l'aventure (explorer + expérimenter)
Hippolyte Lesseliers

   * comgraph, touche à tout, envie de créer de l'image fixe et animé, désir d'explorer Blender. 
Antoine Schmitter

Scénographie: Travail sur les décors, en y intégrant de l'animation, photo réalisme, impression 3D, passer d'une image 2D à un univers 3D.

Justine Brissard M0 Design du livre: lier papier et numérique model 3D dans web, aimation, photoréalisme? ...

Kevin Maoukola - Cinéma d’animation :  modélisation , animation 2D/3D, rigging, low-poly 

Andrea González

   * Design graphique et photo: expérimentation, tissues et fluids




**TODO**:

    - contact sophie du fablab



session 01



- présentation FZ

- présentation du cours



## REFERENCES

- Goodbye uncanny valley: [https://vimeo.com/237568588](https://vimeo.com/237568588)

- [https://frederikheyman.com/](https://frederikheyman.com/)

- zero one: [https://vimeo.com/278407486](https://vimeo.com/278407486)

- Meat Dept.: [https://vimeo.com/meatdept](https://vimeo.com/meatdept)

- Shared memories: [https://vimeo.com/290322470](https://vimeo.com/290322470)

- Nikita Diakur: [https://vimeo.com/nikitadiakur](https://vimeo.com/nikitadiakur)

- Joost Eggermont: [http://www.joosteggermont.nl](http://www.joosteggermont.nl)

- Frederik Vanhoutte [https://twitter.com/wblut](https://twitter.com/wblut)



[https://sisyphus.technofle.sh/](https://sisyphus.technofle.sh/)



all likes by frankiezafe: [https://vimeo.com/frankiezafe/likes/](https://vimeo.com/frankiezafe/likes/)



caméra tracking: [https://www.youtube.com/watch?v=lY8Ol2n4o4A](https://www.youtube.com/watch?v=lY8Ol2n4o4A)



## RACCOURCIS:

G: translate

R: rotate

S: scale



filtrage des axes:

x: sélection de l'axe X

y: sélection de l'axe Y

...



SHIFT + x: tout sauf l'axe X



pour toute les transformations, utiliserle numpad pour entrer les valeurs précise



ENTER: confirmer la modification

ESC: annuler la modification



CURSOR 3D:

    déplacer le curseur 3D: SHIFT + S



SELECTION:

    A = tout sélectionner

    AA = tout déselectionner



DESTRUCTION:

    X



OBJECT MODE / EDIT MODE:

    TAB



SELECTION:

B: sélection rectangulaire

   * click gauche: ajouter à la séction
   * click milieu: enlever à la sélection
C: sélection pinceau

   * click gauche: ajouter à la séction
   * click milieu: enlever à la sélection


E: extrusion



F: ajouter une face ou un edge



SHIFT + D: duplicate



## SVG EXPORT:



- activer le module Freestyle SVG Exporter via Edit > Preferences > Add-ons



dans le Property Editor: 

    - Render properties: cocher Freestyle Export \& Freestyle

    - Output properties: modifier le dossier Output
